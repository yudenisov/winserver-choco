$http_prefix='http'
$host_domain='file.netip4.ru'
$http_port='80'
$project_folder='/Exponenta/'
$project_file='ExponentaMainFilesSetup.exe'

$uninstall_folder=$env:PUB1
$make_uninstall_filename='unins000.exe'
$inno_uninstall_filename='unins001.exe'

$packageName = 'exponenta.main.files'
$package_varname="PUB1"
$softwarenamepattern="Exponenta Admin Packages Main Files *"
$packageName1 = 'Exponenta Admin Packages Main Files'
$silentArgs = '/VERYSILENT /NOCANCEL'
$validExitCodes = @(0)
$installerType = 'exe'

$url = $http_prefix + '://' + $host_domain + ':' + $http_port + $project_folder + $project_file
$make_uninstall_file=$uninstall_folder + '\' + $make_uninstall_filename
$inno_uninstall_file=$uninstall_folder + '\' + $inno_uninstall_filename

$checksum = 'B9CBC9408A6A2807EE9975DCD33E963E'

$ErrorActionPreference = 'Stop'

