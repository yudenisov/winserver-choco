$http_prefix='http'
$host_domain='file.netip4.ru'
$http_port='80'
$project_folder='/Exponenta/'
$project_file='rms.host.6.10.ru_unsigned.msi'

$uninstall_folder=$env:Util
$make_uninstall_filename='make_uninstall.AdminScripts.cmd'

$package_varname="SystemRoot"
$softwarenamepattern="Remote Manipulator System *"
$packageName = 'rmshost.winserver'
$packageName1 = 'Remote Manipulator System - Host'
$installerType = 'msi'
$silentArgs = '/norestart /QN'
$validExitCodes = @(0)

$url = $http_prefix + '://' + $host_domain + ':' + $http_port + $project_folder + $project_file
$make_uninstall_file=$uninstall_folder + '\' + $make_uninstall_filename

$checksum = 'A71EF4FEE7BC726DE6B203B4B0E73308'

$ErrorActionPreference = 'Stop'

